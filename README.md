# Storage Images for Docker

These images provide common storage server types for Docker, currently
`webdav`:

```
docker run -d -p 8080:80 -v "$PWD:/data" -e "password.myusername=password123" codeberg.org/momar/storage:webdav
```


