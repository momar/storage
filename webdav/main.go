package main

import (
	"fmt"
	"golang.org/x/net/webdav"
	"net/http"
	"os"
	"strings"
)

func main() {
	handler := webdav.Handler{
		FileSystem: webdav.Dir("."),
		LockSystem: webdav.NewMemLS(),
	}

	auth := map[string]string{}
	for _, variable := range os.Environ() {
		parts := strings.SplitN(variable, "=", 2)
		if strings.HasPrefix(parts[0], "password.") {
			auth[strings.TrimPrefix(parts[0], "password.")] = parts[1]
		}
	}
	fmt.Printf("%d credentials found\n", len(auth))
	if len(auth) == 0 {
		fmt.Printf("Add credentials by providing environment variables of the following format: password.<username>=<password>\n")
		fmt.Printf("Server error: no credentials found\n")
		os.Exit(1)
	}

	http.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		username, providedPassword, authenticated := req.BasicAuth()
		if actualPassword, exists := auth[username]; !authenticated || !exists || actualPassword != providedPassword {
			body := []byte("Unauthorized")
			res.Header().Set("WWW-Authenticate", "Basic realm=\"WebDAV\"")
			res.Header().Set("Content-Type", "text/plain")
			res.Header().Set("Content-Length", fmt.Sprint(len(body)))
			res.WriteHeader(http.StatusUnauthorized)
			_, _ = res.Write(body)
			return
		}
		handler.ServeHTTP(res, req)
	})

	addr := envOr("HOST", "[::]") + ":" + envOr("PORT", "80")
	fmt.Printf("Listening on http://%s\n", addr)
	err := http.ListenAndServe(addr, nil)
	if err != nil {
		fmt.Printf("Server error: %s\n", err)
		os.Exit(1)
	}
}

func envOr(env string, or string) string {
	if v := os.Getenv(env); v != "" {
		return v
	}
	return or
}
